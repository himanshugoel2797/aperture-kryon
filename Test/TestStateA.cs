using System;
using Kryon2D.Graphics;
using Aperture2D.Math;
using Kryon2D;
using Kryon2D.General;

namespace Test
{
	public class TestStateA : IGameObject
	{
		ResourceScript script;
		Sprite s;
		TileMap t;
		
		public TestStateA ()
		{
		}

		#region IGameObject implementation
		public void Initialize (StateSystem CallingSceneManager)
		{
			script = new ResourceScript();
			script.Load("vfs0:/Application/levelA.txt");
			
			s = new Sprite("FSQA");
			t = (TileMap)script["tileMap"];
		}

		public void Update (long elapsedTime)
		{
			s.World = Matrix.CreateScale(50,50,50);
			Vector2 pos = s.Position;
			Vector2 mapPos = t.Position;
			
			float speed = 0.01f;
			
			if(Input.IsKeyDown(Buttons.Down)){
				pos.Y-=0.2f;
				mapPos.Y += speed;
			}
			if(Input.IsKeyDown(Buttons.Up)){
				pos.Y+=0.2f;
				mapPos.Y -= speed;
			}
			if(Input.IsKeyDown(Buttons.Left)){
				pos.X-=0.2f;
				mapPos.X -= speed;
			}
			if(Input.IsKeyDown(Buttons.Right)){
				pos.X+=0.2f;
				mapPos.X += speed;
			}
			
			//s.Position = pos;
			t.Position = mapPos;
		}

		public void Render ()
		{
			t.Draw(false);
			s.Draw();
			
			//Dispose();
			//AppMain.states.SetCurrent("Dummy");
		}

		public void Dispose ()
		{
			script.Dispose();
		}
		#endregion
	}
}


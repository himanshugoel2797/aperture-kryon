using System;
using System.Collections.Generic;
using Aperture2D.Math;
using Kryon2D.Graphics;
using System.Diagnostics;
using Sce.PlayStation.Core.Graphics;
using Sce.PlayStation.Core.Environment;
using Sce.PlayStation.Core.Input;

namespace Test
{
	public class AppMain
	{
		public static StateSystem states;
		
		public static void Main (string[] args)
		{
			Stopwatch w = Stopwatch.StartNew();
			
			Initialize ();

			while (true) {
				SystemEvents.CheckEvents ();	//Check for any system events
				Update (w.ElapsedMilliseconds);	//Update the game state
				
				//Restart the timer
				w.Reset();
				w.Start();
				
				Render ();	//Render the game state
			}
		}

		public static void Initialize ()
		{
			//Initialize the engine
			Core.Initialize(16f/9f);
			Core.InitializeProjection();
			
			//Setup the state system
			states = new StateSystem();
			states.Add("Dummy", new DummyScene());
			states.Add("TestStateA", new TestStateA());
			states.SetCurrent("TestStateA");
			
			//Initialize the state machine
			states.Initialize();
		}

		public static void Update (long delta)
		{
			//Update the engine's state
			Core.Update(delta);
			
			//Get the current memory usage
			Console.WriteLine(Core.GetMemoryUsage());
			SystemMemory.Dump();
			GC.Collect();
			
			//Update the scene state machine's state
			states.Update(delta);
		}

		public static void Render ()
		{
			// Clear the screen
			Core.Start();
			
			//Render the state machine's state
			states.Render();
			
			Core.Stop();
		}
	}
}

using System;
using Sce.PlayStation.Core.Graphics;
using System.Collections.Generic;
using System.Collections;

namespace Kryon2D.Graphics
{
	/// <summary>
	/// Texture manager.
	/// </summary>
	public class TextureManager : IDisposable
	{
		/// <summary>
		/// Manages Textures.
		/// </summary>
		/// <value>
		/// The textures.
		/// </value>
		public static TextureManager Textures { get; set; }
		
		/// <summary>
		/// Manages Normals.
		/// </summary>
		/// <value>
		/// The normals.
		/// </value>
		public static TextureManager Normals { get; set; }
		
		Dictionary<string, Texture2D> textures;
		Dictionary<string, string> paths;
		
		/// <summary>
		/// Initializes a new instance of the <see cref="Aperture2D.TextureManager"/> class.
		/// </summary>
		public TextureManager ()
		{
			textures = new Dictionary<string, Texture2D> ();
			paths = new Dictionary<string ,string> ();
		}
		
		/// <summary>
		/// Gets or sets the <see cref="Aperture2D.TextureManager"/> with the specified key.
		/// </summary>
		/// <param name='key'>
		/// Key.
		/// </param>
		public Texture2D this [string key] {
			get {
				if (!textures.ContainsKey (key) && paths.ContainsKey (key)) {
					textures [key] = new Texture2D (VFS.GenerateRealPath (paths [key]), false);	
				}
				return textures [key];
			}
			set {
				textures [key] = value;
			}
		}
		
		/// <summary>
		/// Remove the specified key.
		/// </summary>
		/// <param name='key'>
		/// Key.
		/// </param>
		public void Remove (string key)
		{
			if(textures.ContainsKey(key)){
				textures[key].Dispose();
				textures.Remove (key);	
			}
		}
		
		/// <summary>
		/// Add the specified key and texture.
		/// </summary>
		/// <param name='key'>
		/// Key.
		/// </param>
		/// <param name='tex'>
		/// Texture.
		/// </param>
		public void Add (string key, Texture2D tex)
		{
			textures [key] = tex;	
		}
		
		/// <summary>
		/// Add the specified key and load the texture from the specified path.
		/// </summary>
		/// <param name='key'>
		/// Key.
		/// </param>
		/// <param name='filename'>
		/// Filename.
		/// </param>
		public void Add (string key, string filename)
		{
			paths [key] = filename;	
		}

		#region IDisposable implementation
		public void Dispose ()
		{
			List<string> tmp = new List<string> (textures.Keys);
			
			foreach (string key in tmp) {
				textures [key].Dispose ();
				textures.Remove (key);
			}
		}
		#endregion
	}
}


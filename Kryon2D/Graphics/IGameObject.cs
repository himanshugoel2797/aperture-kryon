using System;

namespace Kryon2D.Graphics
{
	public interface IGameObject
	{
		void Initialize(StateSystem CallingSceneManager);
        void Update(long elapsedTime);
        void Render();
        void Dispose();
	}
}


using System;
using Sce.PlayStation.Core.Imaging;
using Sce.PlayStation.Core.Graphics;
using Aperture2D.Math;

namespace Kryon2D.Graphics
{
	public class Text : IDisposable
	{
		private Image textIMG;
		private Texture2D textTex;
		private FullScreenQuad textquad;
		private string drawn = "";
		private string ID = "";
		
		static string current = ((char)0x00).ToString();
		
		public Text ()
		{
			textIMG = new Image (ImageMode.Rgba, new ImageSize ((int)Core.Screen.Width/2, (int)Core.Screen.Height/2), new ImageColor (1, 1, 1, 0));
			textquad = new FullScreenQuad(new Vector4(0,0,0,0));
			textTex = new Texture2D ((int)Core.Screen.Width/2, (int)Core.Screen.Height/2, false, PixelFormat.Rgba);
			ID = current;
			current = ((char)((int)current[0] + 1)).ToString();
		}
		
		public void DrawString (string toDraw, Font font, Vector2 Location, Vector4 Color)
		{
			if (toDraw != drawn) {
				drawn = toDraw;
				
				textIMG.DrawText (toDraw, new ImageColor ((int)(Color.X * 255), (int)(Color.Y * 255), (int)(Color.Z * 255), (int)(Color.W * 255)), font, new ImagePosition ((int)Location.X/2, (int)Location.Y/2));		
				textTex.SetPixels (0, textIMG.ToBuffer ());
				
				
				TextureManager.Textures[ID] = textTex;
				textquad.SetTexture(ID);
				textquad.Multiplier = 2;
			}
		}
		
		public void Clear ()
		{
			try {
				textIMG.Dispose ();
			} catch (Exception) {
			}
				
			textIMG = new Image (ImageMode.Rgba, new ImageSize ((int)Core.Screen.Width/2, (int)Core.Screen.Height/2), new ImageColor (1, 1, 1, 0));
			drawn = "";
		}
		
		public void Draw ()
		{
			textquad.Draw();
		}
		public void Dispose ()
		{
			textquad.Dispose();
			TextureManager.Textures.Remove(ID);
			textTex.Dispose();
			textIMG.Dispose();
		}
	}
}


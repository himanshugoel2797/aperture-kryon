using System;
using Aperture2D.Math;
using Sce.PlayStation.Core.Graphics;

namespace Kryon2D.Graphics
{
	public static class Core
	{
		#region Public properties
		public static Matrix Projection;
		public static Matrix View;
		public static Rectangle Screen;
		public static GraphicsContext Graphics;
		#endregion
		
		#region Methods
		public static string GetMemoryUsage ()
		{
			long managedMemoryUsage = GC.GetTotalMemory (false);
			
			string line = string.Format ("managedMemory= {0}M {1}K {2}byte", 
			                                    managedMemoryUsage >> 20, (managedMemoryUsage & 0xFFFFF) >> 10, managedMemoryUsage & 0x3FF);
			
			return line;
		}
		
		/// <summary>
		/// Start drawing.
		/// </summary>
		public static void Start()
		{
			Graphics.SetClearColor(0.0f, 0.5f, 1.0f , 0.0f);
			Graphics.Clear();
		}
		
		/// <summary>
		/// Stop and Swap the backbuffer.
		/// </summary>
		public static void Stop()
		{
			Graphics.SwapBuffers();	
		}
		
		public static void Update(long delta)
		{
			Input.Update();	
		}
		#endregion
		
		#region Constructors and Initializers
		static Core ()
		{
			Graphics = new GraphicsContext();
			
			Screen = new Rectangle(0, 0, Graphics.Screen.Width, Graphics.Screen.Height);
			
			TextureManager.Normals = new TextureManager();
			TextureManager.Textures = new TextureManager();
			
			View = Matrix.Identity;
		}
		
		public static void Initialize(float AspectRatio = 16f/9f)
		{
			AspectRatio = 1/AspectRatio;
			int width = (int)Screen.Width;
			int height = (int)(width * AspectRatio);
			
			Graphics.Dispose();
			Graphics = new GraphicsContext(width, height, PixelFormat.Rgba, PixelFormat.Depth16, MultiSampleMode.Msaa2x); 
		
			Graphics.SetBlendFunc(BlendFuncMode.Add, BlendFuncFactor.SrcAlpha, BlendFuncFactor.OneMinusSrcAlpha);
			
			Screen.X = Graphics.GetViewport().X;
			Screen.Y = Graphics.GetViewport().Y;
			Screen.Width = width;
			Screen.Height = height;
		}
		
		public static void InitializeProjection(float width = 0, float height = 0)
		{
			if(width == 0)width = Screen.Width;
			if(height == 0)height = Screen.Height;
			
			float halfH = height/2f;
			float halfW = width/2f;
			
			Projection = Matrix.CreateOrthographicOffCenter(-halfW, halfW, -halfH, halfH, -100, 100);
			
			Screen.X = -halfW;
			Screen.Y = halfH;
			Screen.Width = width;
			Screen.Height = height;
		}
		#endregion
	}
}


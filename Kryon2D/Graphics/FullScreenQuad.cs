using System;
using Aperture2D.Math;

namespace Kryon2D.Graphics
{
	public class FullScreenQuad : IDisposable
	{
		Sprite s;
		
		public Matrix World {
			get{
				return s.World;	
			}
			set{
				s.World = value;
			}
		}
		public float Multiplier {
			get{
				return s.Multiplier;
			}	
			set{
				s.Multiplier = value;	
			}
		}
		
		public Vector2 Position{
			get{
				return s.Position;	
			}
			set{
				s.Position = value;	
			}
		}
		
		public FullScreenQuad (string texKey, string normKey)
		{
			s = new Sprite(texKey, normKey);
			s.World = Matrix.CreateScale(-Core.Screen.Width/2, Core.Screen.Height/2, 0);
		}
		public FullScreenQuad (Vector4 color) { 
			s = new Sprite(color);
			s.World = Matrix.CreateScale(-Core.Screen.Width/2, Core.Screen.Height/2, 0);
		}
		
		public void Draw(bool alpha)
		{
			s.Draw(alpha);	
		}
		public void Draw() { Draw (true); }
		
		public void SetTexture(string texKey){
			s.SetTexture(texKey);
		}
		public void SetNormals(string normKey){
			s.SetNormals(normKey);	
		}
		
		public void Dispose ()
		{
			s.Dispose();
		}
	}
}


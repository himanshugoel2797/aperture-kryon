using System;
using System.IO;
using Aperture2D.Math;

namespace Kryon2D
{
	[Flags]
	public enum Direction{ Up = 1, Down = 2, Left = 4, Right = 8 }
	
	public struct CollisionEventArgs{
		public Vector2 Position {get;set;}
		public Direction CollisionDirection {get;set;}
		public byte CollisionID {get;set;}
	}
	
	public class CollisionMap
	{
		/*
		 * Multi byte collision map, allows registering events to collisions with bytes
		 * Additionally, dynamic objects can be handled by modifying the map at runtime
		 * 
		 * The resolution of the map can be changed however due to memory concerns, for now
		 * we only support one byte per tile.
		 * 
		 * 50x50tilemap = 50*50 bytes for the collision map
		 * Allows for up to 255-2 different registered events
		 * 
		 * DEFAULTS:
		 *  0 = Nothing
		 *  1 = Collide - No passthrough
		 * 	2 - 255 = Registerable collisions
		 * 
		 * Event handler:
		 * 	EventArgs{
		 * 		int xPos, yPos;
		 * 		Direction CollisionDirection;
		 * 		Collision ID
		 *	 }
		 */
		
		Stream s;
		Action<CollisionEventArgs>[] Events;
		
		int w, h;
		
		public CollisionMap (string file, int width, int height)
		{
			s = VFS.OpenFile(file);
			Events = new Action<CollisionEventArgs>[byte.MaxValue];
			w = width;
			h = height;
		}
		
		public void Add(byte Id, Action<CollisionEventArgs> handler)
		{
			Events[Id] += handler;	
		}
		
		public bool CheckCollision(int xpos, int ypos, Direction dir)
		{
			if(dir.HasFlag(Direction.Left))xpos-=1;
			if(dir.HasFlag(Direction.Right))xpos+=1;
			if(dir.HasFlag(Direction.Up))ypos+=1;
			if(dir.HasFlag(Direction.Down))ypos-=1;
			
			s.Seek(	(xpos * w) + ypos, SeekOrigin.Begin);
			int data = s.ReadByte();
			
			if(data > 0){
				Events[data](
					new CollisionEventArgs(){
					Position = new Vector2(xpos, ypos),
					CollisionID = (byte)data,
					CollisionDirection = dir
					});
			}
			return (data > 0);
		}
	}
}


using System;
using System.Collections.Generic;

namespace Kryon2D.Graphics
{
	 public class StateSystem
    {
        private Dictionary<string,IGameObject > _scenes;
        private IGameObject _curScene;


        public StateSystem()
        {
            _scenes = new Dictionary<string, IGameObject>();
        }

        public void Add(string name, IGameObject scene)
        {
            _scenes.Add(name, scene);
        }
        public void Remove(string name)
        {
            _scenes.Remove(name);
        }

        public void SetCurrent(string name)
        {
            _curScene = _scenes[name];
        }

        public void Initialize()
        {
            _curScene.Initialize(this);
        }
        public void Update(long interval)
        {
            _curScene.Update(interval);
        }
        public void Render()
        {
            _curScene.Render();
        }
        public void Dispose()
        {
            _curScene.Dispose();
        }
    }
}


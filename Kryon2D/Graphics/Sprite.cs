using System;
using Aperture2D.Math;
using Sce.PlayStation.Core.Graphics;

namespace Kryon2D.Graphics
{
	public class Sprite : IDisposable
	{
		internal static float[] vertices, texCoords;
		internal static ShaderProgram shader;
		internal static VertexBuffer buffer;
		
		public Texture2D Texture { get; private set; }
		public Texture2D Normals { get; private set; }
		public Vector4 Color {get;set;}
		public Vector2 Position {get;set;}
		public float AngleX {get;set;}
		public float AngleY {get;set;}
		public Matrix World {get;set;}
		public float Multiplier {get;set;}
		public Vector4 AmbientLight {get;set;}
		
		private int mode = 0;
		
		public Sprite (Vector4 Color, string textureKey, string normalKey)
		{
			Multiplier = 0.1f;
			AmbientLight = Vector4.One;
			
			#region Setup VBuffer
			if (vertices == null) {
				vertices = new float[]{
					-1,1,
					-1,-1,
					1,-1,
					1,-1,
					1,1,
					-1,1
				};
			}
			
			if (texCoords == null) {
				texCoords = new float[]{
					1,0,
					1,1,
					0,1,
					0,1,
					0,0,
					1,0
				};
			}
			
			if (shader == null) {
				shader = new ShaderProgram (VFS.GetFileBytes ("vfs1:/shaders/BasicLighting.cgx"));
				
				shader.SetAttributeBinding (0, "a_Position");
				shader.SetAttributeBinding (1, "a_TexCoord");
				
				shader.SetUniformBinding (0, "MaterialColor");
				shader.SetUniformBinding (1, "mode");
				shader.SetUniformBinding (2, "WVP");
				shader.SetUniformBinding (3, "multiplier");
				shader.SetUniformBinding (4, "AmbientT");
			}
			
			if (buffer == null) {
				buffer = new VertexBuffer (6, new VertexFormat[]{VertexFormat.Float2, VertexFormat.Float2});
				buffer.SetVertices (0, vertices);
				buffer.SetVertices (1, texCoords);
			}
			#endregion
			
			#region Setup mode and properties
			this.Color = Color;
			
			if (Color != Vector4.One || Color != Vector4.Zero)
				mode = 1;
			if (textureKey != "") {
				Texture = TextureManager.Textures [textureKey];
				mode = 3;
			}
			
			if (normalKey != "") {
				Normals = TextureManager.Normals [normalKey];
				mode = 2;
			}
			#endregion
		}
		public Sprite (Vector4 color) : this(color, "", ""){}
		public Sprite (string tex) : this(Vector4.One, tex, ""){}
		public Sprite (string tex, string norm) : this(Vector4.One, tex, norm){}
	
		public void SetTexture(string texKey)
		{
			Texture = TextureManager.Textures[texKey];
			mode = (Normals == null)?3:2;
		}
		public void SetNormals(string normalKey){
			Normals = TextureManager.Normals[normalKey];	
			mode = 2;
		}
		
		public void Draw(bool alpha){
			Core.Graphics.SetShaderProgram(shader);
			Core.Graphics.SetVertexBuffer(0, buffer);
			Core.Graphics.SetTexture(0, Texture);
			Core.Graphics.SetTexture(1, Normals);
			
			Core.Graphics.Enable(EnableMode.Blend, alpha);
			
			var WVPTmp = Matrix.CreateTranslation(Position.X, Position.Y, 0) * Matrix.CreateRotationX(AngleX) * Matrix.CreateRotationZ(AngleY) * Matrix.CreateRotationY(MathHelper.ToRadians(180))  * World * Core.View * Core.Projection; 
			Sce.PlayStation.Core.Matrix4 WVP = WVPTmp;
			
			Sce.PlayStation.Core.Vector4 Color = this.Color;
			Sce.PlayStation.Core.Vector4 AmbientTerm = this.AmbientLight;
			
			shader.SetUniformValue (0, ref Color);
			shader.SetUniformValue (1, mode);
			shader.SetUniformValue (2, ref WVP);
			shader.SetUniformValue (3, Multiplier);
			shader.SetUniformValue (4, ref AmbientTerm);
			
			Core.Graphics.DrawArrays (DrawMode.Triangles, 0, 6);
			
			Core.Graphics.SetShaderProgram (null);
			Core.Graphics.SetVertexBuffer (0, null);
			Core.Graphics.SetTexture (0, null);
			Core.Graphics.SetTexture (1, null);
		}
		public void Draw(){ Draw(true); }
		
		public void Dispose ()
		{
			
		}
	}
}


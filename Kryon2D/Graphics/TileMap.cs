using System;
using Sce.PlayStation.Core.Imaging;
using Aperture2D.Math;
using Sce.PlayStation.Core.Graphics;
using Kryon2D.Graphics;
using System.IO;

namespace Kryon2D
{
	public class TileMap
	{
		static FullScreenQuad fsq;
		Tuple<string, bool>[] texNames;
		
		public Matrix World {
			get {
				return fsq.World;	
			}
			set {
				fsq.World = value;
			}
		}

		public Vector2 Position {
			get {
				return fsq.Position;	
			}
			set {
				fsq.Position = value;
			}
		}

		public Vector2 Scale { get; set; }
		
		public TileMap ()
		{
			if (fsq == null) {
				fsq = new FullScreenQuad (Vector4.One);	
			}
			
			texNames = new Tuple<string, bool>[10];
		}
		
		public void AddLayer (int level, string texName, bool onTop)
		{
			texNames [level] = new Tuple<string, bool> (texName, onTop);
		}
		
		public void Draw (int layer)
		{
			World = Matrix.CreateScale (Scale.X, Scale.Y, 0);
			fsq.SetTexture (texNames [layer].Item1);
			fsq.Draw ();
		}
		
		public void Draw (bool top)
		{
			for (int c = 0; c < texNames.Length; c++) {
				if (texNames [c] != null) {
					if (texNames [c].Item2 && top) {
						Draw (c);
					} else if (!texNames [c].Item2 && !top) {
						Draw (c);	
					}
				}
			}
		}
	}
}


using System;
using System.Collections.Generic;

namespace Kryon2D.General
{
	public class ResourceManager<T>:IDisposable where T : IDisposable
	{
		Dictionary<string, T> items;
		
		public ResourceManager ()
		{
			items = new Dictionary<string, T> ();
		}
		
		public T this [string key] {
			get {
				return items [key];	
			}
			set {
				items [key] = value;	
			}
		}
		
		public void Remove (string key)
		{
			if (items.ContainsKey (key)) {
				items [key].Dispose ();
				items.Remove (key);
			}
		}
		
		public void Add(string key, T t)
		{
			items[key] = t;	
		}
		
		public void Dispose()
		{
			List<string> tmp = new List<string> (items.Keys);
			
			foreach (string key in tmp) {
				items[key].Dispose ();
				items.Remove (key);
			}
		}
	}
}


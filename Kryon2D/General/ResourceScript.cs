using System;
using System.IO;
using Kryon2D.Graphics;
using System.Collections.Generic;

namespace Kryon2D.General
{
	//Scripting language used to manage the handling of resources and to provide easily modifiable controls for state machines
	/*
	 * Allow specifying all resources to be initialized from the script (including Sprites, CollisionMaps, Text etc)
	 */
	
	public class ResourceScript : IDisposable
	{
		Dictionary<string, Tuple<string, string>> data;
		Dictionary<string, object> tmp;
		
		public ResourceScript ()
		{
			data = new Dictionary<string, Tuple<string,string>> ();	
			tmp = new Dictionary<string, object> ();
		}
		
		public object this [string key] {
			get {
				return tmp [key];
			}
		}
		
		public void Load (string file)
		{
			using (StreamReader r = new StreamReader(VFS.GenerateRealPath(file))) {
				while (!r.EndOfStream) {
					string line = r.ReadLine ().Trim ();
				
					//Resource declarations
					if (line.StartsWith ("[")) {
						
						line = line.Replace ("[", "");
						string type = line.Split (']') [0];
						line = line.Split (']') [1];
						
						data.Add (line.Split (';') [0], new Tuple<string,string> (type, line.Split (';') [1]));
						
						if (type == "Texture")
							TextureManager.Textures.Add (line.Split (';') [0], line.Split (';') [1]);
						
					} else if (line.StartsWith ("(")) {
						line = line.Replace ("(", "");	
						
						if (line.StartsWith ("TileMap")) {
							
							line = line.Replace ("TileMap,", "");	
							string name = line.Split (')') [0];
							line = line.Split (')') [1];
							
							TileMap tileMap = new TileMap ();
							tileMap.Scale = new Aperture2D.Math.Vector2 (float.Parse (line.Split ('x') [0]), float.Parse (line.Split ('x') [1]));
							
							while (true) {
								line = r.ReadLine ().Trim ();
			
								if (line != "(EndTileMap)") {
									string[] parts = line.Replace("(", "").Replace(")", "").Split (',');
									tileMap.AddLayer (int.Parse (parts [0]), parts [1], (parts [2] == "OnTop"));	
								} else
									break;
							}
							
							tmp [name] = tileMap;
						}
						
					}
					
				}
			}
		}

		public void Dispose ()
		{
			foreach(string key in data.Keys)
			{
				if(data[key].Item1 == "Texture")TextureManager.Textures.Remove(key);	
			}
		}
	}
}


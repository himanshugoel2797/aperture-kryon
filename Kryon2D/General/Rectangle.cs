using System;

namespace Kryon2D
{
	public struct Rectangle
	{
		public float X {get;set;}
		public float Y {get;set;}
		public float Width {get;set;}
		public float Height {get;set;}
		
		public static Rectangle Zero {get;private set;}
		
		public Rectangle(float x, float y, float width, float height) : this(){
			X = x;
			Y = y;
			Width = width;
			Height = height;
		}
	}
}


using System;
using Sce.PlayStation.Core.Input;

namespace Kryon2D
{
	[Flags]
	public enum Buttons : uint
	{
		Left = 1u,
		Up = 2u,
		Right = 4u,
		Down = 8u,
		Square = 16u,
		Triangle = 32u,
		Circle = 64u,
		Cross = 128u,
		Start = 256u,
		Select = 512u,
		L = 1024u,
		R = 2048u,
		Enter = 65536u,
		Back = 131072u
	}
	
	public static class Input
	{
		static GamePadData curData, prevData;
		
		public static float AnalogLeftX 	{get; private set;}
		public static float AnalogLeftY 	{get; private set;}
		public static float AnalogRightX 	{get; private set;}
		public static float AnalogRightY 	{get; private set;}
		
		public static void Update()
		{
			prevData = curData;
			curData = GamePad.GetData(0);
			
			AnalogLeftX = curData.AnalogLeftX;
			AnalogLeftY = curData.AnalogLeftY;
			
			AnalogRightX = curData.AnalogRightX;
			AnalogRightY = curData.AnalogRightY;
		}
		
		public static bool IsKeyDown(Buttons b)
		{
			return (
				curData.ButtonsDown.HasFlag((GamePadButtons)Enum.Parse(typeof(GamePadButtons), b.ToString())) |
				curData.Buttons.HasFlag((GamePadButtons)Enum.Parse(typeof(GamePadButtons), b.ToString()))
				);
		}
		
		public static bool IsKeyPressed(Buttons b){
			return (
				curData.ButtonsDown.HasFlag((GamePadButtons)Enum.Parse(typeof(GamePadButtons), b.ToString()))
				);
		}
	}
}

